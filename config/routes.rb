Rails.application.routes.draw do

  scope module: :v1, constraints: ApiVersion.new('v1', true) do
    resources :properties
    resources :agencies
    resources :ftp_connections
  end

  get "fetch_props", to: "fluxes#fetch_props_from_flux", as: "fetch_props"

end

class XmlError
  include Mongoid::Document
  include Mongoid::Timestamps

  field :error, type: Hash
end

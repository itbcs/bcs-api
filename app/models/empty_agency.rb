class EmptyAgency
  include Mongoid::Document
  include Mongoid::Timestamps

  field :file_name, type: String
end

class Property
  include Mongoid::Document
  include Mongoid::Timestamps

  include ActionView::Helpers::NumberHelper

  belongs_to :agency


  scope :bordeaux, -> { where(ville: { '$in' => ["Bordeaux", "bordeaux"]}) }
  scope :exception, -> { where(prix: {"$gte" => 1000000, "$lte" => 3000000}) }
  scope :cub, -> { where(ville: { '$in' => ["Bordeaux", "bordeaux", "Pessac", "Bègles", "Mérignac"]}) }


  field :name, type: String
  field :localisation, type: String
  field :area, type: Integer
  field :garden_area, type: Integer
  field :field_area, type: Integer
  field :lounge_area, type: Integer
  field :price, type: String
  field :price_alt, type: String
  field :price_int, type: Integer
  field :desc, type: String
  field :status, type: String
  field :status_code, type: Integer
  field :last_update_at, type: Time
  field :source, type:  String
  field :title, type: String
  field :reference, type: String
  field :offer_type, type: String
  field :offer_type_code, type: Integer
  field :corps, type: String
  field :available_date, type: Time
  field :property_type, type: String
  field :property_type_code, type: Integer
  field :nb_room, type: Integer
  field :nb_sleeping_room, type: Integer
  field :nb_sleeping, type: Integer
  field :nb_floor, type: Integer
  field :nb_bathrooms, type: Integer
  field :build_year, type: String
  field :country, type: String
  field :city, type: String
  field :human_city, type: String
  field :zipcode, type: String
  field :fees_target, type: String
  field :fees_target_code, type: Integer
  field :fees, type: String
  field :fees_int, type: Integer

  field :hidden_attributes, type: Hash

  field :hektor_id, type: String
  field :description, type: String
  field :hektor_created_at, type: Date
  field :hektor_updated_at, type: Date
  field :images_array, type: Array
  field :cp, type: String
  field :county, type: String

  field :warrant_type, type: String
  field :warrant_id, type: Integer
  field :warrant_start, type: Date
  field :warrant_end, type: Date

  field :swimming_pool, type: Boolean
  field :parking, type: Boolean
  field :balcony, type: Boolean
  field :elevator, type: Boolean
  field :washroom, type: Boolean
  field :terrace, type: Boolean
  field :garden, type: Boolean

  field :kitchen_type, type: Integer
  field :kitchen_equipment, type: Integer

  field :heating_energy, type: Integer
  field :heating_format, type: Integer
  field :heating_type, type: Integer

  field :ges, type: Integer
  field :dpe, type: Integer
  field :energy_consumption, type: Integer
  field :gas_emission, type: Integer
  field :exposition, type: Integer

  def status_code_humanized
    values = {1 => "En estimation",
              2 => "Actif",
              3 => "Sous offre",
              4 => "Sous compromis",
              5 => "Vendu",
              6 => "Mandat clos"
             }
    if self.status_code
      values[self.property_type_code]
    else
      nil
    end
  end

  def fees_target_code_humanized
    values = {1 => "Acquéreur",
              2 => "Vendeur",
              3 => "Acquéreur et vendeur"
             }
    if self.fees_target_code
      values[self.fees_target_code]
    else
      nil
    end
  end

  def property_type_string
    values = {1 => "Maison",
              2 => "Appartement",
              4 => "Studio",
              5 => "Terrain",
              7 => "Viager",
              10 => "Mas",
              11 => "Bastide",
              15 => "Garage",
              16 => "Parking",
              17 => "Chalet",
              18 => "Duplex",
              20 => "Autre",
              21 => "Immeuble",
              22 => "Propriete",
              23 => "Commerce",
              24 => "Cabanon",
              25 => "Villa",
              26 => "Rez de jardin",
              27 => "Rez de villa",
              28 => "Château",
              29 => "Cave",
              30 => "Ferme",
              32 => "Maison de maître",
              33 => "Maison individuelle",
              34 => "Maison bi-familiale",
              35 => "Fermette",
              36 => "Bungalow",
              37 => "Monastère",
              38 => "Maison de ville",
              39 => "Maison de village",
              40 => "Chambre",
              41 => "Triplex",
              42 => "Penthouse",
              43 => "Terrain à batir",
              44 => "Terrain agricole",
              45 => "Terrain de loisir",
              46 => "Emplacement intérieur",
              47 => "Parking ouvert",
              48 => "Box",
              231 => "Fonds de commerce",
              232 => "Cession de droit au bail",
              235 => "Murs commerciaux",
              2310 => "Bureaux",
              2313 => "Entrepôt",
              2314 => "Local commerciale",
              2315 => "Terrains",
              2316 => "Local d'activité",
              2317 => "Local professionnel"
             }
    if self.property_type_code
      values[self.property_type_code]
    else
      nil
    end
  end


  def self.house_types
    return ["Maison","Maison de village","Ferme","Mas","Villa","Bastide","Rez-de-villa","Echoppe","Propriété","Château"]
  end

  def self.flat_types
    return ["Appartement","Duplex","Immeuble","Loft","Rez-de-jardin","Studio","Triplex"]
  end

  def custom_price
    if self.prix
      "#{number_with_delimiter(self.prix.to_i, locale: :fr)} €"
    else
      "Prix indisponible"
    end
  end

end

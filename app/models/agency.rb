class Agency
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  has_many :properties

  scope :partner, -> { where(partner: true)}
  scope :non_partner, -> { where(partner: false)}


  ## Database authenticatable
  field :email,              type: String, default: ""
  field :encrypted_password, type: String, default: ""
  field :password, type: String, default: ""
  field :password_confirmation, type: String, default: ""

  ## Recoverable
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time

  ## Rememberable
  field :remember_created_at, type: Time

  ## Trackable
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  field :min_price, type: Integer, default: 0
  field :insurer_name, type: String
  field :city, type: String
  field :source, type: String, default: "hektor"
  field :subscription_source, type: String
  field :name, type: String
  field :location, type: String
  field :siret, type: String
  field :url, type: String
  field :logo, type: String
  field :location_cp, type: String
  field :location_y, type:  Float
  field :location_x, type:  Float
  field :agency_key, type:  Integer
  field :phone, type:  String
  field :professional_card, type: String
  field :postal_codes, type: Array
  field :postal_code, type: String
  field :partner, type: Boolean, default: false
  field :activated, type: Boolean, default: false

  field :hektor_id, type: String
  field :website_url, type: String
  field :catchment_area, type: Array
  field :agency_lng, type: Float
  field :agency_lat, type: Float
  field :address, type: String


  def agency_properties_count
    self.properties.count
  end

  def agency_demands_count
    self.demands.count
  end

end

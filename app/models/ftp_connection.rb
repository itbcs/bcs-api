require 'net/ftp'

class FtpConnection
  include Mongoid::Document
  include Mongoid::Timestamps

  field :start_at, type: Time
  field :end_at, type: Time
  field :count, type: String
  field :duration, type: String
  field :new_agency_count, type: Integer
  field :new_properties_count, type: Integer
  field :updated_properties_count, type: Integer
  field :status, type: String, default: "started"
  field :error, type: String

  def message(ftp)
    puts "-------------------------------------"
    puts "-------------------------------------"
    puts "---------- FTP OPEN SUCCESS ---------"
    puts "#{ftp.inspect}"
  end

  def connect
    start_time = Time.zone.now
    ftp = Net::FTP.new("ftp.cluster020.hosting.ovh.net","bientotcsj-hektor","bgisGdAZFvc5")
    self.start_at = start_time
    self.save
    ftp.passive = true

    @xml_parser = XmlParser.new()
    @collection = @xml_parser.get_collection(ftp)
    @results = @xml_parser.get_file(@collection, ftp)

    self.end_at = Time.zone.now
    time_in_seconds = self.end_at - self.start_at
    self.duration = processed_time(time_in_seconds)
    if @results.is_a?(String)
      self.status = "failed"
      self.error = @results
    elsif @results.is_a?(Hash)
      self.new_agency_count = @results[:agencies]
      self.new_properties_count = @results[:new_props]
      self.updated_properties_count = @results[:up_props]
      self.status = "success"
    end
    self.save
  end


  def disconnect(ftp)
    self.end_at = Time.zone.now
    time_in_seconds = self.end_at - self.start_at
    self.duration = processed_time(time_in_seconds)
    self.save
    ftp.close
  end

  def processed_time(time_diff)
    time_diff = time_diff.round.abs
    hours = time_diff / 3600
    dt = DateTime.strptime(time_diff.to_s, '%s').utc
    "#{hours}h:#{dt.strftime "%Mm:%Ss"}"
  end

end

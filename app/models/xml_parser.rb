class XmlParser

  def get_collection(ftp)
    ftp.chdir("./")
    ftp.chdir("xml")
    collection = ftp.nlst('*.xml')
    puts "**** AGENCY COUNT ****"
    puts collection.size
    puts "**********************"
    collection
  end

  def get_file(collection, ftp)
    new_agency_count = 0
    new_properties_count = 0
    updated_properties_count = 0
    collection[0..200].each do |filename|
      begin
        ftp.close
        ftp = Net::FTP.new("ftp.cluster020.hosting.ovh.net","bientotcsj-hektor","bgisGdAZFvc5")
        ftp.chdir("./")
        ftp.chdir("xml")
        binary_file = ftp.getbinaryfile(filename,nil)
      rescue Net::FTPTempError
        FtpError.create(code: "FTPTempError - 421 Timeout", file: "#{filename}")
        return "421 Timeout for #{filename}"
      rescue Net::ReadTimeout
        FtpError.create(code: "ReadTimeout", file: "#{filename}")
        return "Net::ReadTimeout for #{filename}"
      end
      results = format_xml(binary_file,filename)
      new_agency_count += results[:new_agencies_count]
      new_properties_count += results[:new_properties_count]
      updated_properties_count += results[:updated_properties_count]
    end
    values = {agencies: new_agency_count, new_props: new_properties_count, up_props: updated_properties_count}
    return values
  end

  def create_hektor_agency(property, agency_name)
    attributes = {}
    attributes[:hektor_id] = agency_name.to_s
    new_agency = false
    if property
      property.xpath("./*").each do |tag|
        if tag.name == "agenceLocation"
          postal_code = tag.attributes["cp"].value
          agency_lat = tag.attributes["latitude"].value
          agency_lng = tag.attributes["longitude"].value
          address = tag.content
          attributes[:agency_lng] = agency_lng.to_f
          attributes[:agency_lat] = agency_lat.to_f
          attributes[:postal_code] = postal_code.to_s
          attributes[:address] = address.to_s.scrub.strip
          attributes[:postal_codes] = [postal_code.to_s]
        elsif tag.name == "reference_client"
          hektor_id = tag.content
        elsif tag.name == "agence"
          attributes[:name] = tag.content
          siret = tag.attributes["siret"].value
          website_url = tag.attributes["url"].value
          attributes[:website_url] = website_url
          attributes[:siret] = siret
        elsif tag.name == "email"
          email = tag.content.to_s.downcase.strip
          attributes[:email] = email
        elsif tag.name == "agenceLogo"
          logo = tag.content
          attributes[:logo] = logo
        elsif tag.name == "agenceVille"
          city = tag.content
          attributes[:city] = city
        elsif tag.name == "telephone"
          phone = tag.content
          attributes[:phone] = phone
        end
      end
    end

    @agency = Agency.where(email: attributes[:email]).last
    # puts @agency.inspect
    puts attributes
    if !@agency && property
      @password = "bcs_protect_#{agency_name}"
      @new_agency = Agency.new(attributes)
      @new_agency.password = @password
      @new_agency.password_confirmation = @password
      @new_agency.save
      puts "**********************************"
      puts @new_agency.errors.full_messages
      puts @new_agency.persisted?
      if !@new_agency.persisted?
        XmlError.create(error: attributes)
      end
      puts "**********************************"
      new_agency = true
      return [new_agency, @new_agency]
    elsif @agency
      return [new_agency, @agency]
      puts "vuvuvuvuvuvuvuvuvuvuvuvuvuvu"
    elsif !property
      return attributes[:hektor_id].to_s
    end
  end


  def format_xml(binary_file,filename)
    new_agency_count = 0
    count = 0
    @new_prop_count = 0
    @updated_prop_count = 0
    find_agencies = []
    formatted_agency_name = filename.to_s.strip.remove(".xml")
    xml_string_file = Nokogiri::XML(binary_file)
    properties = xml_string_file.xpath("//ad")

    puts "*******************************************"
    puts "Nb de biens : #{properties.size}"
    puts "*******************************************"

    present_agency_array_with_instance = create_hektor_agency(properties.first, formatted_agency_name)

    if present_agency_array_with_instance.is_a? Array

      if present_agency_array_with_instance.first
        new_agency_count += 1
      end

      @agency = present_agency_array_with_instance.last

      properties.each do |property|
        puts "**** ENTIRE XML PROPERTY ****"
        puts property.inspect
        puts "*****************************"
        empty_property_hash = {}
        empty_property_hash[:hidden_attributes] = {}
        empty_property_hash[:images_array] = []
        @test = property.xpath("./*")
        property.xpath("./*").each do |tag|
          puts "**** ORIGINAL TAG VIEW ****"
          puts "#{tag.name} => #{tag.content}"
          puts "***************************"
          if tag.name == "id"
            empty_property_hash[:hektor_created_at] = tag.attributes['dateEnr'].value
            empty_property_hash[:hektor_updated_at] = tag.attributes['dateMaj'].value
            empty_property_hash[:hektor_id] = tag.content.to_s.strip
          elsif tag.name == "exposition"
            puts "**** EXPOSITION ****"
            puts tag.content.to_s.strip
            if tag.content.to_s.strip.parameterize == "sud"
              empty_property_hash[:exposition] = 1
            elsif tag.content.to_s.strip.parameterize == "ouest"
              empty_property_hash[:exposition] = 2
            elsif tag.content.to_s.strip.parameterize == "nord"
              empty_property_hash[:exposition] = 3
            elsif tag.content.to_s.strip.parameterize == "est"
              empty_property_hash[:exposition] = 4
            elsif tag.content.to_s.strip.parameterize == "sud-est"
              empty_property_hash[:exposition] = 5
            elsif tag.content.to_s.strip.parameterize == "sud-ouest"
              empty_property_hash[:exposition] = 6
            elsif tag.content.to_s.strip.parameterize == "nord-est"
              empty_property_hash[:exposition] = 7
            elsif tag.content.to_s.strip.parameterize == "nord-ouest"
              empty_property_hash[:exposition] = 8
            end
          elsif tag.name == "cons_energie"
            puts "**** CONSO ENERGIE ****"
            puts tag.content.to_s.strip
            empty_property_hash[:energy_consumption] = tag.content.to_s.strip.to_i
          elsif tag.name == "class_energie"
            puts "**** CLASS ENERGIE (DPE)****"
            puts tag.content.to_s.strip
            if tag.content.to_s.strip.downcase.to_sym == :a
              empty_property_hash[:dpe] = 1
            elsif tag.content.to_s.strip.downcase.to_sym == :b
              empty_property_hash[:dpe] = 2
            elsif tag.content.to_s.strip.downcase.to_sym == :c
              empty_property_hash[:dpe] = 3
            elsif tag.content.to_s.strip.downcase.to_sym == :d
              empty_property_hash[:dpe] = 4
            elsif tag.content.to_s.strip.downcase.to_sym == :e
              empty_property_hash[:dpe] = 5
            elsif tag.content.to_s.strip.downcase.to_sym == :f
              empty_property_hash[:dpe] = 6
            elsif tag.content.to_s.strip.downcase.to_sym == :g
              empty_property_hash[:dpe] = 7
            end
          elsif tag.name == "emission_gaz"
            puts "**** EMISSION GAZ ****"
            puts tag.content.to_s.strip
            empty_property_hash[:gas_emission] = tag.content.to_s.strip.to_i
          elsif tag.name == "class_emission_gaz"
            puts "**** CLASS EMISSION GAZ (GES)****"
            puts tag.content.to_s.strip
            if tag.content.to_s.strip.downcase.to_sym == :a
              empty_property_hash[:ges] = 1
            elsif tag.content.to_s.strip.downcase.to_sym == :b
              empty_property_hash[:ges] = 2
            elsif tag.content.to_s.strip.downcase.to_sym == :c
              empty_property_hash[:ges] = 3
            elsif tag.content.to_s.strip.downcase.to_sym == :d
              empty_property_hash[:ges] = 4
            elsif tag.content.to_s.strip.downcase.to_sym == :e
              empty_property_hash[:ges] = 5
            elsif tag.content.to_s.strip.downcase.to_sym == :f
              empty_property_hash[:ges] = 6
            elsif tag.content.to_s.strip.downcase.to_sym == :g
              empty_property_hash[:ges] = 7
            end
          elsif tag.name == "images"
            tag.xpath("./*").map{|xtag| empty_property_hash[:images_array].push(xtag.content)}
          elsif tag.name == "prix"
            empty_property_hash[:price] = tag.content.to_s.strip
            empty_property_hash[:price_int] = tag.content.to_s.strip.to_i
          elsif tag.name == "type_bien_code"
            empty_property_hash[:property_type_code] = tag.content.to_s.strip.to_i
          elsif tag.name == "titre"
            empty_property_hash[:title] = tag.content.to_s.strip
          elsif tag.name == "corps"
            empty_property_hash[:corps] = tag.content.to_s.strip
          elsif tag.name == "ville"
            empty_property_hash[:city] = tag.content.to_s.strip
            empty_property_hash[:human_city] = tag.attributes['humanName'].value
          elsif tag.name == "cp"
            empty_property_hash[:zipcode] = tag.content.to_s.strip
          elsif tag.name == "departement"
            empty_property_hash[:county] = tag.content.to_s.strip
          elsif tag.name == "surface_habitable"
            empty_property_hash[:area] = tag.content.to_s.strip.to_i
          elsif tag.name == "chambres"
            empty_property_hash[:nb_sleeping_room] = tag.content.to_s.strip.to_i
          elsif tag.name == "nb_pieces"
            empty_property_hash[:nb_room] = tag.content.to_s.strip.to_i
          elsif tag.name == "status"
            empty_property_hash[:status_code] = tag.content.to_s.strip.to_i
          elsif tag.name == "numero_mandat"
            empty_property_hash[:warrant_id] = tag.content.to_s.strip.to_i
          elsif tag.name == "type_mandat"
            empty_property_hash[:warrant_type] = tag.content.to_s.strip
          elsif tag.name == "datedebut_mandat"
            empty_property_hash[:warrant_start] = tag.content.to_s.strip
          elsif tag.name == "datefin_mandat"
            empty_property_hash[:warrant_end] = tag.content.to_s.strip
          elsif tag.name == "honoraires"
            empty_property_hash[:fees] = tag.content.to_s.strip
            empty_property_hash[:fees_int] = tag.content.to_s.strip.to_i
          elsif tag.name == "honorairesAChargeDe"
            empty_property_hash[:fees_target_code] = tag.content.to_s.strip.to_i
          elsif tag.name == "date_dispo"
            empty_property_hash[:available_date] = tag.content.to_s.strip
          elsif tag.name == "annee_construction"
            if tag.content.to_s.strip != "0"
              empty_property_hash[:build_year] = tag.content.to_s.strip
            end
          elsif tag.name == "surface_jardin"
            empty_property_hash[:garden_area] = tag.content.to_s.strip.to_i
          elsif tag.name == "surface_terrain"
            empty_property_hash[:field_area] = tag.content.to_s.strip.to_i
          elsif tag.name == "surface_sejour"
            empty_property_hash[:lounge_area] = tag.content.to_s.strip.to_i
          elsif tag.name == "chauffage"
            puts "Chauffage tag"
            puts tag.content.to_s.strip
            if tag.content.to_s.strip.downcase.strip == "radiateur"
              empty_property_hash[:heating_type] = 1
            elsif tag.content.to_s.strip.downcase.strip == "au sol"
              empty_property_hash[:heating_type] = 2
            elsif tag.content.to_s.strip.downcase.strip == "air pulse"
              empty_property_hash[:heating_type] = 3
            elsif tag.content.to_s.strip.downcase.strip == "convecteur"
              empty_property_hash[:heating_type] = 4
            elsif tag.content.to_s.strip.downcase.strip == "poele"
              empty_property_hash[:heating_type] = 5
            elsif tag.content.to_s.strip.downcase.strip == "panneaux rayonnant"
              empty_property_hash[:heating_type] = 6
            end
            puts "-------------"
          elsif tag.name == "energie_chauffage"
            puts "Energie chauffage tag"
            puts tag.content.to_s.strip
            if tag.content.to_s.strip.downcase.strip == "gaz"
              empty_property_hash[:heating_energy] = 1
            elsif tag.content.to_s.strip.downcase.strip == "electrique"
              empty_property_hash[:heating_energy] = 2
            elsif tag.content.to_s.strip.downcase.strip == "fioul"
              empty_property_hash[:heating_energy] = 3
            elsif tag.content.to_s.strip.downcase.strip == "charbon"
              empty_property_hash[:heating_energy] = 4
            elsif tag.content.to_s.strip.downcase.strip == "bois"
              empty_property_hash[:heating_energy] = 5
            elsif tag.content.to_s.strip.downcase.strip == "solaire"
              empty_property_hash[:heating_energy] = 6
            elsif tag.content.to_s.strip.downcase.strip == "geothermie"
              empty_property_hash[:heating_energy] = 7
            elsif tag.content.to_s.strip.downcase.strip == "pompe a chaleur"
              empty_property_hash[:heating_energy] = 8
            elsif tag.content.to_s.strip.downcase.strip == "climatisation"
              empty_property_hash[:heating_energy] = 9
            elsif tag.content.to_s.strip.downcase.strip == "mixte"
              empty_property_hash[:heating_energy] = 10
            elsif tag.content.to_s.strip.downcase.strip == "sans"
              empty_property_hash[:heating_energy] = 11
            elsif tag.content.to_s.strip.downcase.strip == "autre"
              empty_property_hash[:heating_energy] = 12
            end
          elsif tag.name == "format_chauffage"
            puts "Format chauffage tag"
            puts tag.content.to_s.strip
            if tag.content.to_s.strip.downcase.strip == "collectif"
              empty_property_hash[:heating_format] = 1
            elsif tag.content.to_s.strip.downcase.strip == "individuel"
              empty_property_hash[:heating_format] = 2
            elsif tag.content.to_s.strip.downcase.strip == "central"
              empty_property_hash[:heating_format] = 3
            end
          elsif tag.name == "piscine"
            puts "Swimming pool tag"
            puts tag.content.to_s.strip
            if tag.content.to_s.strip.to_i == 1
              empty_property_hash[:swimming_pool] = true
            end
            puts "-----------------"
          elsif tag.name == "parking"
            puts "Parking tag"
            puts tag.content.to_s.strip
            if tag.content.to_s.strip.to_i == 1
              empty_property_hash[:parking] = true
            end
            puts "-----------------"
          elsif tag.name == "balcon"
            puts "Balcony tag"
            puts tag.content.to_s.strip
            if tag.content.to_s.strip.to_i == 1
              empty_property_hash[:balcony] = true
            end
            puts "-----------------"
          elsif tag.name == "ascenseur"
            puts "Elevator tag"
            puts tag.content.to_s.strip
            if tag.content.to_s.strip.downcase == "oui"
              empty_property_hash[:elevator] = true
            end
            puts "-----------------"
          elsif tag.name == "buanderie"
            puts "Washroom tag"
            puts tag.content.to_s.strip
            if tag.content.to_s.strip.to_i == 1
              empty_property_hash[:washroom] = true
            end
            puts "-----------------"
          elsif tag.name == "terrasse"
            puts "Terrace tag"
            puts tag.content.to_s.strip
            if tag.content.to_s.strip.to_i == 1
              empty_property_hash[:terrace] = true
            end
            puts "-----------------"
          elsif tag.name == "jardin"
            puts "Garden tag"
            puts tag.content.to_s.strip
            if tag.content.to_s.strip.to_i == 1
              empty_property_hash[:garden] = true
            end
            puts "-----------------"
          elsif tag.name == "cuisine"
            puts "Kitchen tag"
            kitchen_values = tag.content.to_s.strip.split("-")
            if kitchen_values.size == 1
              if kitchen_values.first.downcase.strip == "americaine"
                empty_property_hash[:kitchen_type] = 1
              elsif kitchen_values.first.downcase.strip == "kitchenette"
                empty_property_hash[:kitchen_type] = 2
              elsif kitchen_values.first.downcase.strip == "separee"
                empty_property_hash[:kitchen_type] = 3
              elsif kitchen_values.first.downcase.strip == "sans"
                empty_property_hash[:kitchen_type] = 4
              end
            else
              if kitchen_values.first.downcase.strip == "americaine"
                empty_property_hash[:kitchen_type] = 1
              elsif kitchen_values.first.downcase.strip == "kitchenette"
                empty_property_hash[:kitchen_type] = 2
              elsif kitchen_values.first.downcase.strip == "separee"
                empty_property_hash[:kitchen_type] = 3
              elsif kitchen_values.first.downcase.strip == "sans"
                empty_property_hash[:kitchen_type] = 4
              end
              if kitchen_values.last.downcase.strip == "equipee"
                empty_property_hash[:kitchen_equipment] = 1
              elsif kitchen_values.last.downcase.strip == "semi equipee"
                empty_property_hash[:kitchen_equipment] = 2
              end
            end
            puts "-----------------"
          elsif tag.name == "environnement"
            puts "Environnement tag"
            puts tag.content.to_s.strip
          end
          # empty_property_hash[tag.name.to_sym] = tag.content.to_s.scrub("*").strip

          # if tag.attributes != {}
          #   empty_property_hash[:hidden_attributes][tag.name.to_sym] = tag.attributes.map{|key, value| [value.name, value.value]}
          # end
        end

        empty_property_hash.delete(:id)
        empty_property_hash[:source] = "hektor"
        empty_property_hash[:country] = "france"
        # puts empty_property_hash

        @prop = @agency.properties.where(hektor_id: empty_property_hash[:hektor_id]).last

        if !@prop
          @hektor_property = Property.create(empty_property_hash)
          handle_flux_fields(empty_property_hash)
          @hektor_property.agency = @agency
          @hektor_property.save

          puts "**** PROPERTY INSTANCE ****"
          puts @hektor_property.inspect
          puts "***************************"

          @agency.properties.push(@hektor_property)
          @agency.save
          @new_prop_count += 1
          # puts "Nouveau bien crée"
        else
          # puts "Bien présent en base !!!!!!!!!!!!!!!!!!!!!!!!!!!"

          handle_flux_fields(empty_property_hash)

          puts "**** PROPERTY INSTANCE ****"
          puts @prop.inspect
          puts "***************************"

          if [Date.today - 5.days..Date.today].include? Date.parse(empty_property_hash[:hektor_updated_at])
            @prop.update_attributes(empty_property_hash)
            @updated_prop_count += 1
            # puts "*************************************************"
            # puts "Mise a jour de bien !!!!!!!!!"
            # puts "*************************************************"
          end
        end

      end
    else
    end

    if present_agency_array_with_instance.is_a? Array
      @new_catchment_area = @agency.properties.map{|property| property.cp.to_s.strip if property.cp }.uniq
      @agency.catchment_area = @new_catchment_area.to_a
      @agency.save
    else
      present_empty_agency = EmptyAgency.where(file_name: present_agency_array_with_instance).last
      if !present_empty_agency
        EmptyAgency.create(file_name: present_agency_array_with_instance)
      end
    end
    values = {new_agencies_count: new_agency_count, new_properties_count: @new_prop_count, updated_properties_count: @updated_prop_count}
    return values
  end

  def handle_flux_fields(property_hash)
    puts "**** PROPERTY HASH ****"
    property_hash.each do |key,value|
      puts "- Key: #{key}"
      puts "- Value: #{value}"
    end
  end



end

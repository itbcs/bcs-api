class FtpError
  include Mongoid::Document
  include Mongoid::Timestamps

  field :code, type: String
  field :file, type: String

end

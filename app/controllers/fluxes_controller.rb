class FluxesController < ApplicationController

  def fetch_props_from_flux
    @ftp_connection = FtpConnection.create()
    @ftp_connection.connect
    render json: @ftp_connection, status: :ok
  end

end

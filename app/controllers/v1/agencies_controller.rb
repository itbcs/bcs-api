class V1::AgenciesController < ApplicationController

  def show
    @agency = Agency.find(params[:id])
    @props = @agency.properties
    json_response({agency: @agency, properties: @props})
  end

  def index
    sorted_params = handle_params
    if sorted_params.size != 0
      @agencies = generate_db_query(sorted_params)
    else
      @agencies = Agency.all
    end
    json_response(@agencies)
  end

  private

  def generate_db_query(filtered_params)
    filtered_params.each do |fp|
      if fp.has_key?(:field)
        if @agencies
          if fp.has_key?(:options)
            @agencies = @agencies.where("#{fp[:field]}" => {"#{fp[:operator]}" => fp[:value], "$options" => "#{fp[:options]}"})
          else
            @agencies = @agencies.where("#{fp[:field]}" => {"#{fp[:operator]}" => fp[:value]})
          end
        else
          if fp.has_key?(:options)
            @agencies = Agency.where("#{fp[:field]}" => {"#{fp[:operator]}" => fp[:value], "$options" => "#{fp[:options]}"})
          else
            @agencies = Agency.where("#{fp[:field]}" => {"#{fp[:operator]}" => fp[:value]})
          end
        end
      end
    end
    @agencies
  end

  def handle_params
    values = []
    potential_params = ["name","city","email","api_key"]
    potential_params.each do |param|
      if params.has_key?(param.to_sym)
        if param.to_sym == :name
          values << {value: params[param.to_sym], "#{param}" => params[param.to_sym], field: "name", operator: "$regex", options: "imx"}
        elsif param.to_sym == :city
          values << {value: params[param.to_sym], "#{param}" => params[param.to_sym], field: "city", operator: "$regex", options: "imx"}
        elsif param.to_sym == :email
          values << {value: params[param.to_sym], "#{param}" => params[param.to_sym], field: "email", operator: "$regex", options: "imx"}
        end
      end
    end
    values
  end


end

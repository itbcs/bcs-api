class V1::PropertiesController < ApplicationController
  require 'will_paginate/array'

  def show
    puts params
    @property = Property.find(params[:id].to_s)

    if !params[:api_key]
      json_response("Unauthorized", 401)
    elsif params[:api_key]
      if params[:api_key] == "ea5f04afe43b502a653a9a27b3aa9688"
        if @property
          json_response(@property)
        else
          json_response({data: "Property not found"})
        end
      end
    end

  end

  def index
    puts "**** PARAMS FORMAT ****"
    puts params.inspect
    puts "***********************"

    sorted_params = handle_params(params)

    if sorted_params.size != 0
      sorted_params.each do |param_hash|
        if param_hash.has_key?(:field)
          if param_hash.has_key?(:operator)
            if @properties
              if param_hash.has_key?(:query_format)
                @properties = @properties.where("#{param_hash[:field]}" => {"#{param_hash[:operator]}" => param_hash[:value].to_i})
              else
                @properties = @properties.where("#{param_hash[:field]}" => {"#{param_hash[:operator]}" => param_hash[:value]})
              end
            else
              if param_hash.has_key?(:query_format)
                @properties = Property.where("#{param_hash[:field]}" => {"#{param_hash[:operator]}" => param_hash[:value].to_i})
              else
                @properties = Property.where("#{param_hash[:field]}" => {"#{param_hash[:operator]}" => param_hash[:value]})
              end
            end
          else
            if @properties
              if param_hash.has_key?(:query_format)
                @properties = @properties.where("#{param_hash[:field]}" => param_hash[:value].to_i)
              else
                @properties = @properties.where("#{param_hash[:field]}" => param_hash[:value])
              end
            else
              if param_hash.has_key?(:query_format)
                @properties = Property.where("#{param_hash[:field]}" => param_hash[:value].to_i)
              else
                @properties = Property.where("#{param_hash[:field]}" => param_hash[:value])
              end
            end
          end
        end
      end
      puts "**** SORTED PARAMS RESULTS WITH RESULTS ****"
      puts @properties.size
      puts "*******************************"
    else
      @properties = Property.all
      puts "**** SORTED PARAMS RESULTS WITHOUT RESULTS ****"
      puts @properties.size
      puts "*******************************"
    end


    if !params[:api_key]
      json_response("Unauthorized", 401)
    elsif params[:api_key]
      if params[:api_key] == "ea5f04afe43b502a653a9a27b3aa9688"
        if @properties.size == 0
          json_response({empty: "Désolé, pas de biens correspondants"})
        elsif @properties.size >= 5000
          json_response({data: "Désolé, trop de biens correspondants"})
        else
          if params[:page]
            json_response(@properties.paginate(page: params[:page], per_page: 25))
          else
            json_response(@properties)
          end
        end
      end
    end

  end

  def handle_params(params)
    values = []
    potential_params = ["status","type","county","min_price","max_price","min_area","max_area","city","zipcode","api_key"]
    potential_params.each do |param|
      if params.has_key?(param.to_sym)
        if param.to_sym == :status
          values << {value: params[param.to_sym], "#{param}" => params[param.to_sym], field: "status_code", query_format: "integer"}
        elsif param.to_sym == :type
          values << {value: params[param.to_sym], "#{param}" => params[param.to_sym], field: "property_type_code"}
        elsif param.to_sym == :county
          values << {value: params[param.to_sym], "#{param}" => params[param.to_sym], field: "county"}
        elsif param.to_sym == :min_price
          values << {value: params[param.to_sym], "#{param}" => params[param.to_sym], field: "price_int", operator: "$gte", query_format: "integer"}
        elsif param.to_sym == :max_price
          values << {value: params[param.to_sym], "#{param}" => params[param.to_sym], field: "price_int", operator: "$lte", query_format: "integer"}
        elsif param.to_sym == :min_area
          values << {value: params[param.to_sym], "#{param}" => params[param.to_sym], field: "area", operator: "$gte", query_format: "integer"}
        elsif param.to_sym == :max_area
          values << {value: params[param.to_sym], "#{param}" => params[param.to_sym], field: "area", operator: "$lte", query_format: "integer"}
        elsif param.to_sym == :city
          values << {value: params[param.to_sym], "#{param}" => params[param.to_sym], field: "city", operator: "$regex"}
        elsif param.to_sym == :zipcode
          values << {value: params[param.to_sym], "#{param}" => params[param.to_sym], field: "zipcode"}
        end
      end
    end
    puts "**** HANDLE PARAMS METHOD ****"
    puts values.inspect
    puts "******************************"
    return values
  end

  private

  def safe_params
    params.permit(:min_price, :max_price, :zipcode, :county, :min_area, :max_area, :city, :type, :status, :api_key, :page)
  end


end
